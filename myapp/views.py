from django.shortcuts import render, redirect, get_object_or_404
from .forms import SignUpForm, UploadArtForm, CommentForm, UserInfo, artTheftForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .models import User, Art, UserFollow, Comment, UserDescription, ArtTheftModel
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt,csrf_protect
from django.contrib.admin.views.decorators import staff_member_required
from django.core import serializers
from django.views.generic.edit import UpdateView

#Index page
def index(request):

    #returns artwork to the front page from users that are not banned
    art = Art.objects.filter(user_id__in = User.objects.filter(is_banned = "0").values('id'))
    return render(request, 'index.html', {'arts' : art})

def banned(request): #redirects to the banned page
    return render(request, 'banned.html')

def register(request):#registers a user
    if request.method == 'POST':
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/login')
    else:
        form = SignUpForm()

    return render(request, 'register.html', {'form': form})

def user_login(request): #logs user into the system

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)

        if user:
            if user.is_active and user.is_banned == "0": 
                login(request, user)   
                return redirect('index')

            else:
                return redirect('banned')
                
    return render(request, 'login.html')

def user_logout(request): #logs out the user and returns them to the main page
    logout(request)
    return HttpResponseRedirect('/')

@login_required #upload artwork for logged in user
def upload_art(request):
    if request.method == 'POST':
        art = UploadArtForm(request.POST, request.FILES)
        if art.is_valid():
            arts = art.save(commit=False)
            arts.user = request.user
            arts.save()
            return redirect('/artwork/' + str(arts.id))
    else: 
        art = UploadArtForm()

    return render(request, 'upload.html', {'art': art})
    
@csrf_exempt
def artwork(request, pk): #displays artwork to the user
    arts = Art.objects.get(pk=pk)
    profile_user = User.objects.get(id = arts.user_id)
   
    if request.is_ajax():
        print(request.POST['loggedid'], ' ', request.POST['artid'])
        comments = Comment(user = User.objects.get(id = request.user.id), comment = request.POST['comment'], artwork = Art.objects.get(id = request.POST['artid']))
        comments.save()
        comment = Comment.objects.filter(artwork_id = arts.id).select_related().order_by('id').reverse()
        user = {
            'id': request.POST['loggedid']
        }
        html = render_to_string('comment.html', context = {'profile_user': profile_user, 'comment': comment, 'user': user, 'arts': arts})
        print(html)
        return HttpResponse(html)

    comment = Comment.objects.filter(artwork_id = arts.id).select_related().order_by('id').reverse()
    return render(request, 'artwork.html', context = {'profile_user': profile_user, 'arts':arts, 'comment': comment})

def profile(request, pk): #displays a users profile page
    if request.user.is_authenticated:
        print(request.user)
        profile_user = User.objects.get(id = pk)
        if(profile_user.is_banned != "0"): #Checks if a user is banned
            return HttpResponseRedirect('/banned')

        else:
            art = Art.objects.filter(user_id = profile_user.id)
            followers = UserFollow.objects.filter(follower = profile_user.id).select_related()

            follows = UserFollow.objects.filter(follower = User.objects.get(id = pk),following = User.objects.get(id = request.user.id)).exists()
            if(UserDescription.objects.filter(user_id = profile_user.id).exists()):
                description = UserDescription.objects.get(user_id = profile_user.id)
                print("aaa")
                print(description)
            else:
                description = False

            return render(request, 'profile.html', context = {'profile_user' : profile_user, 'art' : art, 'follows': follows, 'followers': followers, 'description': description})
    else:
        return HttpResponseRedirect('/register')

@login_required
def artTheft(request, pk):
    #displays art theft page
    if request.method == 'POST':
        artTheftDetail = artTheftForm(request.POST)
        print(request.POST)
        if artTheftDetail.is_valid():
            artTheftCommit = artTheftDetail.save(commit=False)
            artTheftCommit.art = Art.objects.get(pk=pk)
            artTheftCommit.save()
    else:
        artForm = artTheftForm()
        return render(request, 'arttheft.html', {'art':artForm})

    return HttpResponseRedirect('/')

def bio(request):
    #A page that has a form for a user to display their bio
    if request.method == 'POST':
        bio = UserInfo(request.POST)
        print(request.POST)
        if bio.is_valid():
            if UserDescription.objects.filter(user_id = request.user.id).exists():
                    UserDescription.objects.filter(user_id = request.user.id).update(bio = request.POST['bio'], gender = request.POST['gender'], hobby = request.POST['hobby'], fav_artist = request.POST['fav_artist'], website = request.POST['website'])
            else:
                bios = bio.save(commit=False)
                bios.user = request.user
                bios.save()
                return redirect('/profile/' + str(request.user.id))

    else: bio = UserInfo()

    return render(request, 'bio.html', {'bio': bio})
    
def delete(request, pk):
    #A function that deletes a users comment from an artwork
    Comment.objects.filter(id = pk).delete()
    comment = Comment.objects.filter(artwork_id = request.POST['artid']).order_by('id').reverse()
    profile_user = User.objects.get(id = request.POST['userid'])
    user = {
            'id': request.POST['loggedid']
        }
    html = render_to_string('comment.html', context = {'profile_user': profile_user, 'comment': comment, 'user': user})
    return HttpResponse(html)


@staff_member_required
def reports(request): # a page that displays the reports generated by users which is only viewed by admin
    artTheftList = ArtTheftModel.objects.all().select_related()
    
    return render(request, 'reports.html', { 'artTheftList': artTheftList })


@csrf_exempt
def follow(request): #function to allow users to follow a user
    if UserFollow.objects.filter(follower = User.objects.get(id = request.POST['pk']),following = User.objects.get(id = request.user.id)).exists():
        print("Already added")
    else:
        userFollow = UserFollow(follower = User.objects.get(id = request.POST['pk']), following = User.objects.get(id = request.user.id))
        userFollow.save()

    return JsonResponse({"nothing actually": "needs to be returned"})

@csrf_exempt
def unfollow(request):#function to allow users to unfollow a user
    if UserFollow.objects.filter(follower = User.objects.get(id = request.POST['pk']),following = User.objects.get(id = request.user.id)).exists():
        UserFollow.objects.filter(follower = User.objects.get(id = request.POST['pk']),following = User.objects.get(id = request.user.id)).delete()
    else:
        print("Already unfollowed")

    return JsonResponse({"its just": "django wants request"})

def following(request, pk): #displays a page that shows the who the user is following
    following = UserFollow.objects.filter(following = User.objects.get(id = pk)).select_related()
    for x in following :
        print(x.following.id)
    return render(request, 'following.html', { 'following' : following })
    

@csrf_exempt
def makeVisible(request): #ajax call to make comment visible
    Comment.objects.filter(id = request.POST['pk']).update(visible = "1")
    return JsonResponse({"nothing actually": "needs to be returned"})

def makeInvisible(request): #ajax call to make comment invisible
    Comment.objects.filter(id = request.POST['pk']).update(visible = "0")
    return JsonResponse({"nothing actually": "needs to be returned"})

def notFollow(request, pk): #function to allow users to unfollow from the following page
    x = request.user.id
    print(x)
    UserFollow.objects.filter(follower = User.objects.get(id = pk)).delete()
    return redirect('/profile/following/'+ str(x))

def user_update(request, pk, template_name = 'register.html'): #update a users info
    user = get_object_or_404(User, pk = pk)
    form = SignUpForm(request.POST or None, instance = user)
    if form.is_valid():
        form.save()
        return redirect('profile/'+str(pk))
    return render(request, template_name, {'form': form})


def bio_update(request, pk, template_name = 'bio.html'): #update a users bio
    if(request.user.id != UserDescription.objects.get(user_id = pk).id):
        return HttpResponseRedirect('/')
    else:
        user = get_object_or_404(UserDescription, pk = pk)
        form = UserInfo(request.POST or None, instance = user)
        if form.is_valid():
            form.save()
            return redirect('/profile/' + str(request.user.id))
        return render(request, template_name, {'bio': form})