from django.contrib import admin
from .models import User, Art, UserFollow, Comment, UserDescription, ArtTheftModel

admin.site.register(User)
admin.site.register(Art)
admin.site.register(UserFollow)
admin.site.register(Comment)
admin.site.register(UserDescription)
admin.site.register(ArtTheftModel)
