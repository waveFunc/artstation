from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from .models import Art, Comment, UserDescription, ArtTheftModel

CHOICES = [
    (1, 'Yes'),
    (0, 'No')
]

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length = 30, required = False, help_text = "Optional.", )
    last_name = forms.CharField(max_length = 30, required = False, help_text = "Optional.")
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    image = forms.ImageField()

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'image')

class UploadArtForm(forms.ModelForm):
    title = forms.CharField(max_length = 30, required = True, widget = forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(max_length = 100, required = True, widget = forms.TextInput(attrs={'class': 'form-control'}))
    drawing = forms.ImageField()
    moderate = forms.CharField(label = "Moderate Comments:", widget = forms.RadioSelect(choices=CHOICES))

    class Meta:
        model = Art
        fields = ('title', 'description', 'drawing', 'moderate')


class CommentForm(forms.ModelForm):
    comment = forms.CharField(max_length = 100, required = True, widget = forms.Textarea)
    class Meta:
        model = Comment
        fields = ('comment',)

class UserInfo(forms.ModelForm):
    bio = forms.CharField(max_length = 200, widget = forms.TextInput(attrs={'class': 'form-control'}))
    gender = forms.ChoiceField(widget = forms.RadioSelect, choices = (('Male', 'Male'), ('Female', 'Female')))
    hobby = forms.CharField(max_length = 100, widget = forms.TextInput(attrs={'class': 'form-control'}))
    fav_artist  = forms.CharField(max_length = 50, widget = forms.TextInput(attrs={'class': 'form-control'}))
    website = forms.CharField(max_length =50, widget = forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = UserDescription
        fields = ('bio', 'gender', 'hobby', 'fav_artist', 'website')

class artTheftForm(forms.ModelForm):
    description = forms.CharField(max_length = 300, required = True, widget = forms.Textarea)

    class Meta:
        model = ArtTheftModel
        fields = ('description',)