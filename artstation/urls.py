"""artstation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from myapp import views
from django.conf import settings
from django.conf.urls.static import static  

urlpatterns = [
    url(r'^$', views.index, name = "index"),
    url(r'^register/', views.register, name = "signup"),
    url(r'^login/', views.user_login, name = "login"),
    url(r'^logout/',views.user_logout, name = "logout"),
    url(r'^profile/(?P<pk>\d+)/', views.profile, name = "profile"),
    url(r'^upload/', views.upload_art, name = "Upload"),
    url(r'^artwork/(?P<pk>\d+)/', views.artwork, name = "artwork"),
    url(r'^follow', views.follow, name='follow'),
    url(r'^unfollow', views.unfollow, name='unfollow'),
    url(r'^notFollow/(?P<pk>\d+)/', views.notFollow, name='notFollow'),
    url(r'^makevisible',views.makeVisible, name = "makeVisible"),
    url(r'^makeinvisible', views.makeInvisible, name = "makeInvisible"),
    url(r'^bio', views.bio, name='bio'),
    url(r'^delete/(?P<pk>\d+)/', views.delete, name = "delete"),
    url(r'^arttheft/(?P<pk>\d+)/', views.artTheft, name = "artTheft"),
    url(r'^reports/', views.reports, name = "reports"),
    url(r'^profile/following/(?P<pk>\d+)/', views.following, name = "following"),
    url(r'^edit/(?P<pk>\d+)/', views.user_update, name = "user_update"),
    url(r'^editBio/(?P<pk>\d+)', views.bio_update, name = "bio_update"),
    url(r'^banned/', views.banned, name = "banned"),
    url(r'^admin/', admin.site.urls), 
]  

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

